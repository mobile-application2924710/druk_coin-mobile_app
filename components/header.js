import { View,Text,Dimensions,StyleSheet,Image } from "react-native";
// import { LinearGradient } from 'expo-linear-gradient';

function HeaderPage(){
    return(
        <View style={styles.HeaderContainer}>
            <Image source={require("../assets/images/dragon.png")} style={styles.backImage} />
            <Text style={{fontSize:30,fontWeight:'bold',color:'#D9D9D9'}}>DrukCoin</Text>
        </View>
    )
}

export default HeaderPage;

const { width, height } = Dimensions.get('window');

const styles=StyleSheet.create({
    HeaderContainer:{
        height:"35%",
        backgroundColor:"#429690",
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center', 
        gap:10
    },
    backImage:{
        height:"60%",
        width:"60%",
        opacity:0.5,
    }

})