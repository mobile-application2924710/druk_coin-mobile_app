import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore';
import 'firebase/compat/auth';
import 'firebase/compat/storage';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { initializeAuth, getReactNativePersistence} from 'firebase/auth/react-native';
import { getFirestore } from 'firebase/firestore';
import { getApps, getApp } from 'firebase/app';

const firebaseConfig = {
  apiKey: "AIzaSyA_o3MrUeBzT8etg5cRgEV05-6-SV3EjqE",
  authDomain: "mobile-application-5aeaf.firebaseapp.com",
  projectId: "mobile-application-5aeaf",
  storageBucket: "mobile-application-5aeaf.appspot.com",
  messagingSenderId: "61401037232",
  appId: "1:61401037232:web:6138d53d8474057b04fe75",
  measurementId: "G-XMBCWZZ0MR"
};

let app;
if (!getApps().length) {
    app = firebase.initializeApp(firebaseConfig);
}

app = getApp(); 

const auth = initializeAuth(app, {
    persistence: getReactNativePersistence(AsyncStorage)
    });

const db = getFirestore(app);
    
export { db, auth, firebase };
