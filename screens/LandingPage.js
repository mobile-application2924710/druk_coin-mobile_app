import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const LandingPage = ({ navigation }) => {

  return (
    <View style={styles.container}>
      <Image source={require('../assets/images/logo.png')} style={styles.logo} />
      <View style={{marginTop:"10%"}}>
            <Text style={styles.title}>Welcome To</Text>
            <Text style={styles.title}>DrukCoin</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Login')}>
                <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Signup')}>
                <Text style={styles.buttonText}>Signup</Text>
            </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: windowWidth * 0.1,
  },
  logo: {
    width: windowWidth * 0.7,
    height: windowHeight * 0.3,
    resizeMode: 'contain',
    marginTop:'-40%',
  },
  title: {
    fontSize: windowWidth * 0.07,
    fontWeight: '500',
    // marginTop: windowHeight * 0.05,
    // marginBottom: windowHeight * 0.1,
    // textAlign: 'center',
  },
  button: {
    backgroundColor: '#187B75',
    padding: windowHeight * 0.025,
    borderRadius: 5,
    marginTop: windowHeight * 0.05,
    minWidth: windowWidth * 0.7,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: windowWidth * 0.05,
    textAlign: 'center',
  },
});

export default LandingPage;
