import { useIsFocused } from '@react-navigation/native';
import { View,Text,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView, TextInput,} from 'react-native';
// import { Icon } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/Ionicons';
import {getAuth,updateEmail,sendEmailVerification, createUserWithEmailAndPassword,signInWithEmailAndPassword,signOut} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs,  } from '@firebase/firestore';
import { db } from '../config';
import { useState,useEffect} from "react";
import {firebase} from '../config';
import firestore from '@react-native-firebase/firestore';


function ProfilePage({navigation}){

  const [name, setName] = useState('');
  const [email, setEmail] = useState('')
  const [name1, setName1] = useState('');
  const [email1, setEmail1] = useState('')
  const [isEditing, setIsEditing] = useState(false);

  const [data, setItemArray] = useState('');
  const itemListCollection = firebase.firestore().collection('users');

  const auth = getAuth()
  const user = auth.currentUser;

  useEffect ( () => {
      itemListCollection
      .onSnapshot(
        querySnapshot => {
          const itemArray = []
          querySnapshot.forEach((doc) => {
            const {email,name} = doc.data()
            itemArray.push({
              id:doc.id,
              email,
              name
            })
          })
          setItemArray(itemArray)
        })
    },[])

    useEffect (() => {
      console.log(data)
      for (let i = 0; i < data.length; i++) { 
        if(user.uid == data[i].id){
          setName(data[i].name) 
          setName1(data[i].name)
          console.log("Name from here:",data[i].name)
              
        }
      }
    },[data])
    const handleEditName = () => {
      setIsEditing(true);
    };

    const handleEditEmail = () => {
      setIsEditing(true);
    };
  
    const handleSavePress = () => {
      if (name == "") {
        setName(name1);
      }
      if (email == "") {
        setEmail(email1);
      }
      if (email != email1) {
        updateEmail(user, email)
          .then(() => {
            console.log('Email address changed successfully.');
            sendEmailVerification(user, {
              handleCodeInApp: true,
              url: 'https://mobile-application-5aeaf.firebaseapp.com',
            })
              .then(() => {
                console.log('Verification email sent successfully.');
              })
              .catch(error => {
                console.error('Error sending verification email:', error);
              });
          })
          .catch(error => {
            console.error('Error changing email address:', error);
          });
      }
      if (name != name1) {
        firebase.firestore().collection('users')
        .doc(user.uid)
        .update({
          email: email,
          name:name
        })
        .then(() => {
          console.log('User updated!');
        })
        .catch((error) => {
          console.log(error)
        });
      } 
      setIsEditing(false);
    };
    const handleLogout = () => {
      // Perform logout logic here
      try {
        console.log("Log Out function")
      auth.signOut()
      .then(()=>{
        console.log("User logged out successfully.");
        navigation.navigate('LandingPage')
        })
      .catch((error)=>{
        alert(error.message)
        })
      } catch (error) {
        alert(error.message)
      }
      
    }

    const isFocused = useIsFocused();

    useEffect(() => {
      setIsEditing(false);
      if (user) {
        setEmail1(user.email)
        setEmail(user.email)
        setName1(name)
        console.log(name)
      }
    }, [isFocused]);

    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={styles.nav}>
                <Text style={{textAlign:'center',fontSize:22,
                    fontWeight:"500",color:'white',}}>Profile </Text>
                <TouchableOpacity style={styles.logoutButton} onPress={handleLogout}>
                  <Icon name="log-out-outline" size={30} color="white" />
                </TouchableOpacity>
            </View>
            <View style={styles.form}>
              <View style={{width: 100,
                  height: 100,
                  borderRadius: 50,
                  backgroundColor:'#cccc',
                  justifyContent:"center",
                  alignItems:"center",}}>
                <Icon name="person" size={50} color="#fff" />
              </View>
              
              <View style={{ width:"80%",marginTop:"7%"}}>
                <View style={styles.field}>
                  <Icon name="person" size={20} color="#888" style={styles.icon} />
                  <TextInput
                    value={name}
                    placeholder={name1}
                    onChangeText={setName}
                    editable={isEditing}
                    style={styles.input}
                  />
                  {!isEditing && (
                    <TouchableOpacity onPress={handleEditName}>
                      <Icon name="pencil" size={20} color="#000" style={styles.editIcon} />
                    </TouchableOpacity>
                  )}
                </View>
                <View style={styles.field}>
                    <Icon name="mail" size={20} color="#888" style={styles.icon} />
                    <TextInput
                      value={email}
                      placeholder={email1}                      
                      onChangeText={setEmail}
                      editable={isEditing}
                      style={styles.input}
                    />
                    {!isEditing && (
                      <TouchableOpacity onPress={handleEditEmail}>
                        <Icon name="pencil" size={20} color="#000" style={styles.editIcon} />
                      </TouchableOpacity>
                    )}
                  </View>
                  {isEditing && (
                    <TouchableOpacity onPress={handleSavePress} style={styles.saveButton}>
                      <Icon name="save" size={20} color="#fff" style={styles.saveIcon} />
                    </TouchableOpacity>
                  )}
              </View>
            </View>
        </KeyboardAvoidingView>

    )
}
export default ProfilePage;

const styles=StyleSheet.create({
    container: {
        flex: 1,
    },
    nav: {
      height: 150,
      backgroundColor: '#187B75',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      gap: 85,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 10,
    },
    form:{
      marginTop:"5%",
      justifyContent:"center",
      alignItems:"center",
    },
    field: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 10,
      borderWidth:1,
      borderRadius:20
    },
    input: {
      flex: 1,
      borderColor: '#888',
      paddingVertical: 8,
      paddingHorizontal: 10,
    },
    icon: {
      marginLeft: 10,
    },
    editIcon: {
      marginRight: 10,
    },
    saveButton: {
      backgroundColor: '#187B75',
      paddingVertical: 10,
      paddingHorizontal: 20,
      alignSelf: 'flex-end',
      borderRadius: 5,
      marginRight:20,marginTop:5
    },
    saveIcon: {
      alignSelf: 'center',
    },

})

