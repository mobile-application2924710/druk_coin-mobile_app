import { View,Text,TextInput,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView, Platform , Alert} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {getAuth, createUserWithEmailAndPassword,signInWithEmailAndPassword,} from 'firebase/auth';
import { collection, addDoc } from 'firebase/firestore';
import { doc, query, where, getDocs,  } from 'firebase/firestore';
import { db } from '../config';
import { useState,useEffect} from "react";
import {firebase} from '../config'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function ExpenseDetails({navigation,route}){

    const id = route.params.itemId
    const auth = getAuth()
    const today = new Date();
    const user = auth.currentUser;
    let day = today.getDate();
    let month = today.getMonth() + 1;
    let year = today.getFullYear();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    const [name, setName] = useState('');
    const [amount, setAmount] = useState('');
    const [des, setExpenseDes] = useState('');

    useEffect ( () => {
        let value = route.params.value;
        setName(value);
      },[])
    

    const handleGoBack = () => {
      // Handle navigation to previous page here
      navigation.goBack();
    };

    
    const [data, setItemArray] = useState('');
    const [data2, setItemArray2] = useState('');
    const itemListCollection1 = firebase.firestore().collection('budget');
    const itemListCollection2 = firebase.firestore().collection('expense');

    useEffect ( () => {
        itemListCollection1
        .onSnapshot(
          querySnapshot => {
            const itemArray = []
            querySnapshot.forEach((doc) => {
              const {budget,month,user,year} = doc.data()
              itemArray.push({
                id: doc.id,
                user,
                budget,
                month,
                year
              })
            })
            setItemArray(itemArray)
          })
      },[])
    let budget

    useEffect ( () => {
        itemListCollection2
        .onSnapshot(
          querySnapshot => {
            const itemArray = []
            querySnapshot.forEach((doc) => {
              const {day,month,time,user,expenseName,expenseAmount,expenseDes} = doc.data()
              itemArray.push({
                id: doc.id,
                day,
                month,
                year,
                time,
                user,
                expenseName,
                expenseAmount,
                expenseDes
              })
            })
            setItemArray2(itemArray)
          })
      },[])

    let expenseTotal =Number("0");

    for (let i = 0; i < data.length; i++) {
        if(user.uid == data[i].user){
            if(month==data[i].month && year==data[i].year){
                budget = data[i].budget;
            }
        }
    }

    for (let i = 0; i < data2.length; i++) {
        if(user.uid == data2[i].user){
            if(month==data2[i].month && year==data2[i].year){
                expenseTotal = expenseTotal + Number(data2[i].expenseAmount)
            }
        }
    }
    let balance = budget-expenseTotal;

    let expenseName
    let expenseAmount
    let expenseDes
    let expenseDay
    let expenseMonth
    let expenseYear
    let expenseTime

    for (let i = 0; i < data2.length; i++) {
        if(data2[i].id == id){
            expenseName = data2[i].expenseName
            expenseAmount = data2[i].expenseAmount
            expenseDes = data2[i].expenseDes
            expenseDay = data2[i].day
            expenseMonth = data2[i].month
            expenseYear = data2[i].year
            expenseTime = data2[i].time
        }
    }

    return (
        <KeyboardAvoidingView style={styles.container}  >
            <View style={styles.nav}>
                <TouchableOpacity onPress={handleGoBack} style={styles.backButton}>
                    <Icon name="arrow-back" size={24} color="white" />
                </TouchableOpacity>
                <Text style={{textAlign:'center',fontSize:20,
                    fontWeight:"bold",color:'white',}}> Expense Details
                </Text>
            </View>
            <View style={styles.balanceExpenseContainer}>
                <View style={styles.balance}>
                    <Text style={{textAlign:'center',fontSize:18,
                    fontWeight:"400",color:'white',}}>Your Balance: Nu.{balance?balance:"00.00"}</Text>
                </View>
                <View style={styles.expense}>
                    <Text style={{textAlign:'center',fontSize:18,
                    fontWeight:"400",color:'white'}}>Total Expense: Nu.{expenseTotal?expenseTotal:"00.00"}</Text>
                </View>
            </View>
            
            <View style={styles.form}>
                <View style={styles.NameContainer}>
                    <Text style={{fontSize:20,fontWeight:'500'}}>Expense Name:</Text>
                    <TextInput value={expenseName}
                    style={{borderBottomWidth:1,width:'20%',fontSize:20}}/>
                </View>
                <View style={styles.AmountContainer}>
                    <Text style={{fontSize:20,fontWeight:'500'}}>Expense Amount:</Text>
                    <Text style={{borderBottomWidth:1,width:'15%',fontSize:20,}}>{expenseAmount}</Text>
                </View>
                <View style={styles.DateContainer}>
                    <Text style={{fontSize:20,fontWeight:'500'}}>Expense Made on:</Text>
                    <Text style={{borderBottomWidth:1,width:'55%',fontSize:20,}}>{expenseDay}/{expenseMonth}/{expenseYear} at {expenseTime}</Text>
                </View>
                <View style={styles.DescriptionContainer}>
                    <Text style={{fontSize:20,fontWeight:'500'}}>Expense Description:</Text>
                    <Text style={{fontSize:20}}>{expenseDes}</Text>
                </View>
            </View>
        </KeyboardAvoidingView>
        

    )
}
export default ExpenseDetails;

const styles=StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor:"#429690"
    },
    nav:{
        // borderWidth:1,
        height:150,
        backgroundColor:'#187B75',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:25,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20
        // marginTop:20
        
    },
    sidebarMenu:{
        // borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    balanceExpenseContainer:{
        // borderWidth:1,
        marginTop:'5%',
        width:"90%",
        marginLeft:'5%',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:35

    },
    balance:{
        // borderWidth:1,
        width:"45%",
        borderRadius:22,
        height:110,
        backgroundColor:'#429690',
        justifyContent:'center',
        alignItems:'center',
        padding:20,shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30,
        
    },
    expense:{
        // borderWidth:1,
        width:"45%",
        height:110,
        borderRadius:22,
        backgroundColor:'#429690',
        justifyContent:'center',
        alignItems:'center',
        padding:20,shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30,
    },
    form:{
        marginTop:"6%",
        paddingTop:30,
        backgroundColor:'#FFFFFF',
        borderTopStartRadius:30,
        borderTopEndRadius:30,
        width: '100%',
        alignItems: 'center',
    },
    input: {
        paddingVertical: windowHeight * 0.015,
        paddingHorizontal: windowWidth * 0.05,
        borderRadius: 10,
        marginTop: windowHeight * 0.03,
        width: windowWidth * 0.8,
        fontSize: windowWidth * 0.04,
        color: '#333',
        borderColor: '#ccc',
        borderBottomWidth: 1,
        padding:20,
    },
    button: {
        backgroundColor: '#187B75',
        padding: windowHeight * 0.016,
        borderRadius: 5,
        marginTop: windowHeight * 0.05,
        minWidth: windowWidth * 0.8,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: windowWidth * 0.05,
        textAlign: 'center',
    },
    backButton: {
        position: 'absolute',
        left: 25,
      },
      NameContainer:{
        display:'flex',
        flexDirection:"row",
        gap:20
      }, 
      AmountContainer:{
        display:'flex',
        flexDirection:"row",
        gap:20,
        marginTop:20
      },
      DateContainer:{
        marginTop:20,
      },
      DescriptionContainer:{
        marginTop:20,
        marginBottom:40,
        display:'flex',justifyContent:'center',alignItems:'center'
      }

})