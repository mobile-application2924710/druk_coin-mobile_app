import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import { Badge } from 'react-native-elements';
import React, { useState, useEffect } from 'react';
import { getAuth } from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs } from '@firebase/firestore';
import { db } from '../config';
import { firebase } from '../config';

import HomePage from '../screens/HomePage';
import ChartPage from '../screens/Chart';
import ProfilePage from '../screens/Profile';
import NotificationPage from '../screens/Notification';
import AddExpensePage from '../screens/AddExpense';

const Tab = createBottomTabNavigator();

function MyTabs() {
  const auth = getAuth();
  const today = new Date();
  const user = auth.currentUser;
  let month = today.getMonth() + 1;
  let year = today.getFullYear();

  const [data, setData] = useState([]);
  const [data2, setData2] = useState([]);
  const itemListCollection1 = firebase.firestore().collection('budget');
  const itemListCollection2 = firebase.firestore().collection('expense');

  useEffect(() => {
    const unsubscribe1 = itemListCollection1.onSnapshot((querySnapshot) => {
      const itemArray = [];
      querySnapshot.forEach((doc) => {
        const { budget, month, user, year } = doc.data();
        itemArray.push({
          id: doc.id,
          user,
          budget,
          month,
          year,
        });
      });
      setData(itemArray);
    });

    const unsubscribe2 = itemListCollection2.onSnapshot((querySnapshot) => {
      const itemArray = [];
      querySnapshot.forEach((doc) => {
        const { day, month, time, user, expenseName, expenseAmount, expenseDes } = doc.data();
        itemArray.push({
          id: doc.id,
          day,
          month,
          year,
          time,
          user,
          expenseName,
          expenseAmount,
          expenseDes,
        });
      });
      setData2(itemArray);
    });

    return () => {
      unsubscribe1();
      unsubscribe2();
    };
  }, []);

  const [expenseTotal, setExpenseTotal] = useState(0);
  const [budget, setBudget] = useState(null);

  useEffect(() => {
    let total = 0;

    for (let i = 0; i < data2.length; i++) {
      if (user.uid === data2[i].user && month === data2[i].month && year === data2[i].year) {
        total += Number(data2[i].expenseAmount);
      }
    }

    setExpenseTotal(total);
  }, [data2]);

  useEffect(() => {
    let budgetValue = null;

    for (let i = 0; i < data.length; i++) {
      if (user.uid === data[i].user && month === data[i].month && year === data[i].year) {
        budgetValue = Number(data[i].budget);
      }
    }

    setBudget(budgetValue);
  }, [data]);

  const renderNotificationBadge = () => {
    if (budget === null ) {
      return <Badge value="" status="error" />;
    }

    return null;
  };

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'Chart') {
            iconName = focused ? 'analytics' : 'analytics-outline';
          } else if (route.name === 'Profile') {
            iconName = focused ? 'person' : 'person-outline';
          } else if (route.name === 'Notification') {
            iconName = focused ? 'notifications' : 'notifications-outline';
          }

          return (
            <>
              <Icon name={iconName} size={size} color={color} />
              {route.name === 'Notification' && renderNotificationBadge()}
            </>
          );
        },
        headerShown: false, // Hide the default header
      })}
      tabBarOptions={{
        activeTintColor: '#429690',
        inactiveTintColor: 'gray',
      }}
    >
      <Tab.Screen name="Home" component={HomePage} />
      <Tab.Screen name="Notification" component={NotificationPage} />
      <Tab.Screen name="Chart" component={ChartPage} />
      <Tab.Screen name="Profile" component={ProfilePage} />
      {/* <Tab.Screen name="AddExpense" component={AddExpensePage}/> */}
    </Tab.Navigator>
  );
}

export default MyTabs;
